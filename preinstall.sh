#!/bin/bash

# Updates
sudo apt update
sudo apt dist-upgrade -y

# Software found in normal repos and third-partxy software installers
sudo apt install software-properties-common software-properties-gtk synaptic flatpak -y
sudo apt-key adv --recv-key 4773BD5E130D1D45
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt update
sudo apt remove firefox firefox-esr epiphany-browser konqueror chromium -y
sudo apt autoremove -y
sudo apt install inkscape mumble transmission-gtk git pcscd samba virtualbox preload vlc spotify-client falcon -y
sudo systemctl enable --now pcscd
sudo systemctl enable --now preload
sudo modprobe vboxdrv
sudo sed -i "s/NoDisplay=true/NoDisplay=false/g" /etc/xdg/autostart/*.desktop
sleep 3
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak update -y
sudo flatpak install flathub org.telegram.desktop im.riot.Riot com.bitwarden.desktop com.github.calo001.fondo -y

# Steam
wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb
sudo dpkg -i steam.deb
sudo apt install -fy
rm steam.deb

# MEGA downloads
wget https://mega.nz/linux/MEGAsync/Debian_10.0/amd64/megasync-Debian_10.0_amd64.deb

# MEGA package
sudo dpkg -i megasync-Debian_10.0_amd64.deb
sudo apt install -fy
rm megasync-Debian_10.0_amd64.deb

# Vulkan and Mesa drivers
sudo dpkg --add-architecture i386
sudo apt install libgl1-mesa-dri:i386 -y
sudo apt install mesa-vulkan-drivers mesa-vulkan-drivers:i386 -y

# Wine repo keys
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
rm winehq.key

# Wine repo and software
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/debian/ buster main' -y
sudo apt update
sudo apt install --install-recommends wine-stable -y

# Lutris
echo "deb http://download.opensuse.org/repositories/home:/strycore/Debian_9.0/ ./" | sudo tee /etc/apt/sources.list.d/lutris.list
wget -q https://download.opensuse.org/repositories/home:/strycore/Debian_9.0/Release.key -O- | sudo apt-key add -
sudo apt update
sudo apt install lutris -y

# AppImageLauncher
wget -O appimagelauncher.deb https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.1.0/appimagelauncher_2.1.0-travis897.d1be7e7.bionic_amd64.deb
sudo dpkg -i appimagelauncher.deb
sudo apt install -fy
rm appimagelauncher.deb

# Yubico Authenticator
mkdir ~/Applications
wget -O ~/Applications/yubioath-desktop-latest-linux.AppImage https://developers.yubico.com/yubioath-desktop/Releases/yubioath-desktop-latest-linux.AppImage

# Visual Studio Code
sudo apt install python3-pip -y
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install --lts
npm install -g yarn phonegap
wget https://az764295.vo.msecnd.net/stable/9579eda04fdb3a9bba2750f15193e5fafe16b959/code_1.41.0-1576089540_amd64.deb
sudo dpkg -i code_1.41.0-1576089540_amd64.deb
sudo apt install -fy
rm code_1.41.0-1576089540_amd64.deb
sudo apt update
sudo apt dist-upgrade -y

# Keybase
wget https://prerelease.keybase.io/keybase_amd64.deb
sudo dpkg -i keybase_amd64.deb
sudo apt install -fy
rm keybase_amd64.deb

# Clean up and autoremoval
sudo apt clean
sudo apt autoremove -y
